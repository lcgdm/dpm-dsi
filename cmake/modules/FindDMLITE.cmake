#
# This module detects if dmlite is installed and determines where the
# include files and libraries are.
#
# This code sets the following variables:
# 
# DMLITE_LIBRARIES   = full path to the dmlite libraries
# DMLITE_INCLUDE_DIR = include dir to be used when using the dmlite library
# DMLITE_FOUND       = set to true if dmlite was found successfully
#
# DMLITE_LOCATION
#   setting this enables search for dmlite libraries / headers in this location


# -----------------------------------------------------
# DMLITE Libraries
# -----------------------------------------------------
find_library(DMLITE_LIBRARIES
    NAMES dmlite
    HINTS ${DMLITE_LOCATION}/lib ${DMLITE_LOCATION}/lib64 ${DMLITE_LOCATION}/lib32
    DOC "The main dmlite library"
)

# -----------------------------------------------------
# DMLITE Include Directories
# -----------------------------------------------------
find_path(DMLITE_INCLUDE_DIR
    NAMES dmlite/c/dmlite.h
    HINTS ${DMLITE_LOCATION} ${DMLITE_LOCATION}/include
    DOC "The dmlite include directory"
)
if(DMLITE_INCLUDE_DIR)
    message(STATUS "dmlite includes found in ${DMLITE_INCLUDE_DIR}")
endif()

# -----------------------------------------------------
# handle the QUIETLY and REQUIRED arguments and set DMLITE_FOUND to TRUE if 
# all listed variables are TRUE
# -----------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(dmlite DEFAULT_MSG DMLITE_LIBRARIES DMLITE_INCLUDE_DIR)
mark_as_advanced(DMLITE_INCLUDE_DIR DMLITE_LIBRARIES)
