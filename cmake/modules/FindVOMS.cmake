#
# This module detects if VOMS is installed and determines where the
# include files and libraries are.
#
# This code sets the following variables:
# 
# VOMS_LIBRARIES      = full path to the voms libraries
# VOMS_INCLUDE_DIR    = include dir to be used when using the voms library
# VOMS_FOUND          = set to true if voms was found successfully
#
# VOMS_LOCATION
#   setting this enables search for voms libraries / headers in this location


# -----------------------------------------------------
# VOMS Libraries
# -----------------------------------------------------
find_library(VOMS_LIBRARIES
    NAMES vomsapi
    HINTS $ENV{VOMS_LOCATION}/lib $ENV{VOMS_LOCATION}/lib64
    PATHS /usr/lib /usr/lib64
    DOC "The voms api library"
)

# -----------------------------------------------------
# VOMS Include Directories
# -----------------------------------------------------
find_path(VOMS_INCLUDE_DIR
    NAMES voms_apic.h
    HINTS $ENV{VOMS_LOCATION}/include $ENV{VOMS_LOCATION}/include/voms
    PATHS /usr/include /usr/include/voms
    DOC "The voms include directory"
)
if(VOMS_INCLUDE_DIR)
    message(STATUS "voms includes found in ${VOMS_INCLUDE_DIR}")
endif()

# -----------------------------------------------------
# handle the QUIETLY and REQUIRED arguments and set VOMS_FOUND to TRUE if 
# all listed variables are TRUE
# -----------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(voms DEFAULT_MSG VOMS_LIBRARIES VOMS_INCLUDE_DIR)
mark_as_advanced(VOMS_INCLUDE_DIR VOMS_LIBRARIES)
