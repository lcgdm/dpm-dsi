%if %{?fedora}%{!?fedora:0} >= 17 || %{?rhel}%{!?rhel:0} >= 7
%global systemd 1
%else
%global systemd 0
%endif

Name:		dpm-dsi
Summary:	Disk Pool Manager (DPM) plugin for the Globus GridFTP server
Version:	1.9.15
Release:	1%{?dist}
License:	ASL 2.0
Group:		Applications/Internet
URL:		https://svnweb.cern.ch/trac/lcgdm/wiki/Dpm
Source0:	%{name}-%{version}.tar.gz
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:		cmake
BuildRequires:		dmlite-devel
BuildRequires:		globus-gridftp-server-devel >= 11.8
BuildRequires:		yum-utils
BuildRequires:		voms-devel

Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(post):		chkconfig
Requires(postun):	initscripts
Requires:		dmlite-libs >= 1.11.0
Requires:		globus-gridftp-server-progs >= 11.8
%if %systemd
BuildRequires:		systemd
%endif

%if %systemd
Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd
%else
Requires(post):     	chkconfig
Requires(preun):    	chkconfig
Requires(preun):    	initscripts
Requires(postun):   	initscripts
%endif

Provides:		DPM-gridftp-server = %{version}-%{release}
Obsoletes:		DPM-gridftp-server <= 1.8.1
Provides:		DPM-DSI = %{version}-%{release}
Obsoletes:		DPM-DSI <= 1.8.1

%description
The dpm-dsi package provides a Disk Pool Manager (DPM) plugin for the 
Globus GridFTP server, following the Globus Data Storage Interface (DSI).

The Disk Pool Manager (DPM) is a lightweight storage solution for grid sites.
It offers a simple way to create a disk-based grid storage element and 
supports relevant protocols (SRM, gridFTP, RFIO) for file 
management and access.

Globus provides open source grid software, including a server implementation
of the GridFTP protocol. This plugin implements the DPM backend specifics 
required to expose the data using this protocol.

%prep
%setup -q -n %{name}-%{version}

%build
%if %systemd
 	%cmake . -DCMAKE_INSTALL_PREFIX=/ -DSYSTEMD_INSTALL_DIR=%{_unitdir} 
%else
	%cmake . -DCMAKE_INSTALL_PREFIX=/
%endif

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

install -p -d -m 755 %{buildroot}%{_localstatedir}/log/dpm-gsiftp

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%if %systemd
%attr(0644,root,root) %{_unitdir}/dpm-gsiftp.service
%else
%{_initrddir}/dpm-gsiftp
%endif
%config(noreplace) %{_sysconfdir}/logrotate.d/dpm-gsiftp
%config(noreplace) %{_sysconfdir}/sysconfig/dpm-gsiftp
%{_libdir}/libglobus_gridftp_server_dmlite*.so*
%{_localstatedir}/log/dpm-gsiftp
%doc LICENSE RELEASE-NOTES

%post
%if %systemd
	/bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
	/sbin/chkconfig --add dpm-gsiftp
%endif
/sbin/ldconfig

%preun
if [ $1 -eq 0 ] ; then
%if %systemd
	/bin/systemctl stop dpm-gsiftp.service > /dev/null 2>&1 || :
	/bin/systemctl --no-reload disable dpm-gsiftp.service > /dev/null 2>&1 || :
%else
	/sbin/service dpm-gsiftp stop > /dev/null 2>&1
	/sbin/chkconfig --del dpm-gsiftp
%endif
fi

%postun
/sbin/ldconfig 
if [ $1 -ge 1 ]; then
%if %systemd
        /bin/systemctl try-restart dpm-gsiftp.service > /dev/null 2>&1 || :
%else
	/sbin/service dpm-gsiftp condrestart > /dev/null 2>&1 || :
%endif
fi

%changelog
* Wed May 03 2017 Andrey Kiryanov <Andrey.Kiryanov.@cern.ch> - 1.9.13-1
- New Globus API from https://github.com/globus/globus-toolkit/pull/98

* Wed Mar 15 2017 Andrea Manzi <amanzi@cern.ch> - 1.9.12-1
- fix dome checksum when invoked via SRM

* Wed Aug 24 2016 Andrey Kiryanov <kiryanov@cern.ch> - 1.9.9-1
- support for new checksum API

* Mon Aug 01 2016 Andrea Manzi <andrea.manzi@cern.ch> - 1.9.8-1
- Systemd support
- support for gridftp 11.1

* Thu Jan 07 2016 Andrey Kiryanov <andrey.Kiryanov.@cern.ch> - 1.9.7-1
- fix for Gridftp Redirection: transfers with checksums fail when delayed passive is enabled
- fix for Gridftp Redirection: transfer overwrite  fails
- fix for Gridftp logs not compressed
- Implemented checksum calculation on teh disknode with Gridftp Redirection enabled

* Tue Sep 01 2015 Andrea Manzi <andrea.manzi@cern.ch> - 1.9.5-8
- new tag cause new version of globus-gridftp (8.7.1)

* Thu Jul 20 2015 Andrea Manzi <andrea.manzi@cern.ch> - 1.9.5-6
- new tag cause new version of globus-gridftp (8.0)

* Thu Apr 02 2015 Fabrizio Furano <fabrizio.furano@cern.ch> - 1.9.4-3
- Require globus-gridftp-server-progs%{?_isa} >= 7.20

* Mon Sep 29 2014 Andrey Kiryanov <andrey.kiryanov@cern.ch> - 1.9.4-1
- Update for Globus 6 release

* Thu Aug 15 2013 David Smith <david.smith@cern.ch> - 1.9.3-1
- Update for new upstream release

* Fri Jun 28 2013 David Smith <david.smith@cern.ch> - 1.9.2-1
- Update for new upstream release

* Tue Jun 18 2013 David Smith <david.smith@cern.ch> - 1.9.1-1
- Update for new upstream release

* Mon May 06 2013 Alejandro Alvarez <aalvarez@cern.ch> - 1.9.0-2
- Removed devel package

* Wed Dec 19 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.9.0-1
- Update for new upstream release

* Fri Dec 14 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3-1
- Update to new upstream release
- Removed previously required patches (integrated upstream)

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.2-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon Nov 28 2011 Dan Horák <dan[at]danny.cz> - 1.8.2-6
- fix build on 64-bit arches (s390x, sparc64 and possibly others)

* Wed Nov 15 2011 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.2-5
- Removed architecture from globus-gridftp-server-progs requires

* Wed Nov 09 2011 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.2-4
- Added patch for proper libdir usage in ppc64

* Wed Nov 09 2011 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.2-3
- Added patch for LD_LIBRARY_PATH setting in init script

* Fri Nov 04 2011 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.2-2
- Removed README and LICENSE from devel package
- Added parallel flags to make command

* Mon Oct 17 2011 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.2-1
- Initial build
